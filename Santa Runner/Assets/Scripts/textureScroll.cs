﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class textureScroll : MonoBehaviour
{
    public float scrollSpeed = 0.2f;
    public bool scroll = true;

    private Material bGMaterial;


    private void Awake()
    {
        bGMaterial = GetComponent<Renderer>().material;
        
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        if (scroll)
        {
            Vector2 offSet = new Vector2(scrollSpeed * Time.time,0);

            bGMaterial.mainTextureOffset = offSet;
        }
    }
}
