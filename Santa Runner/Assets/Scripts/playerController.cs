﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour
{
    private Rigidbody2D rb;
    private Animator animator;
    private bool isGrounded = false;
    private bool gameOver = false;
    public float jumpForce = 6f;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }
    
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) == true && !gameOver)
        {
            if (isGrounded)
            {
                Jump();
            }
        }
    }

    private void Jump()
    {
        isGrounded = false;
        rb.velocity = Vector2.up * jumpForce;
        animator.SetTrigger("Jump");

        GameManager.instance.AddScore();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            isGrounded = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Obstacle")
        {
            GameManager.instance.GameOver();
            
            Destroy(collision.gameObject);
            animator.Play("SantaDeath");
            gameOver = true;
        }
    }
}
